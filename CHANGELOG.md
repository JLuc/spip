# Changelog

## Unreleased

### Changed

- Les dossiers `ecrire`, `prive`, `squelettes-dist`, `plugins-dist` s'installent avec Composer
- L’écran de sécurité s’installe avec Composer